# Comunidad de Fedora Ecuador

## ¿Qué es Fedora?
El Proyecto Fedora es una comunidad de personas que trabajan **juntas** para
construir una plataforma de software libre y de fuente abierta para
colaborar y compartir soluciones centradas en el usuario basadas en esa plataforma.
O, en lenguaje sencillo, creamos un sistema operativo que facilite hacer cosas útiles.

## ¿De qué se trata Fedora?
La comunidad de Fedora incluye miles de personas con diferentes puntos de vista
y enfoques, pero **juntos** compartimos algunos valores comunes. Llamamos a estos
valores en común las "Cuatro Fundaciones": Libertad, Amigos, Características y Primero.

## Únete a la comunidad y colabora
Para comenzar, regístrate para obtener una cuenta de usuario de Fedora, utilizando el [**Sistema de cuentas de Fedora**](https://admin.fedoraproject.org/accounts/user/new). Puedes empezar por el grupo de interes especifico [**Fedora Join**](https://fedoraproject.org/wiki/SIGs/Join).
Si deseas tomar parte activa mejorando aún más Fedora, hay muchas formas en que puede contribuir. La imaginación es el límite.

## Canales de Comunicación:

#### Telegram
https://t.me/FedoraEC

#### Lista de Correo
https://lists.fedoraproject.org/admin/lists/ec-users.lists.fedoraproject.org/

#### IRC
[#fedora-ec@irc.freenode.net](https://webchat.freenode.net/?channels=#fedora-ec)

## ¿Cómo se organiza Fedora?
Fedora es un gran proyecto, con muchas partes móviles y docenas de grupos y subgrupos.
El cuadro de abajo ofrece una visión general y un sentido de cómo encajan las cosas.
El [Consejo de Fedora](https://docs.fedoraproject.org/en-US/council/) es nuestro
cuerpo general de liderazgo y gobierno. La mayor parte del proyecto se organiza
más o menos según [FESCo](https://docs.fedoraproject.org/fedora-project/subprojects/fesco.html) (el Comité Directivo de Ingeniería de Fedora)
o el [Comité de Mindshare](https://docs.fedoraproject.org/fedora-project/subprojects/mindshare.html).

Dado que muchos grupos son bastante informales, considera que la gráfica no esta
escrita en piedra. Puedes conseguir más información sobre los diversos equipos e
iniciativas en nuestra [documentación de subproyectos](https://docs.fedoraproject.org/fedora-project/subprojects/collaboration.html).

Si todo esto es un poco abrumador, no te preocupes. Elije el área de tu interese,
preséntate a las personas involucradas y comienza. Fedora es una comunidad amigable y abierta, donde todos son bienvenidos.


<p align="center"><img src="https://docs.fedoraproject.org/en-US/project/_images/orgchart.png" /></p>

